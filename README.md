# SQL Projects

This repository contains a collection of custom SQL projects. The projects assume a [Debian](https://www.debian.org/) or [Fedora](https://getfedora.org/) GNU/Linux environment.

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Required Packages (Debian)</th>
      <th>Required Packages (Fedora)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="sqlite">SQLite</a></td>
      <td>A collection of commands and queries using SQLite.</td>
      <td><code>sqlite3</code></td>
      <td><code>sqlite</code></td>
    </tr>
    <tr>
      <td><a href="postgresql">PostgreSQL</a></td>
      <td>A collection of commands and queries using PostgreSQL.</td>
      <td><code>postgresql</code>, <code>postgresql-contrib</code></td>
      <td>N/A</td>
    </tr>
  </tbody>
</table>

## Project Avatar

`logo.png` is [mono sql](https://freesvg.org/mono-sql-65390) by [Free SVG](https://freesvg.org/) and is licensed under a [Copyright-Only Dedication* (based on United States law) or Public Domain Certification](https://creativecommons.org/licenses/publicdomain/) license.